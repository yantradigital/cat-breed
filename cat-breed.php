<?php 
@session_start();

/*

Plugin Name: Cat Breed

Plugin URI: http://www.vantagewebtech.com

Description: This is Cat Breed plugin

Author: Rinku Kamboj

Version: 5.0

Author URI: http://www.vantagewebtech.com

*/
//*************** Admin function ***************
//this function is used for checking login details



function Manage_cat1() {
	include('listquestions.php');
}
function Manage_catslide1() {
	include('listcats.php');
}

if(isset($_REQUEST['cb']) && ($_REQUEST['cb']=='instruction') )
{
	function Manage_instruction() {
		include('instruction.php');
	}
	function managegcat_admin_instruction() {
		add_menu_page("CatBreed", "Cat Breed", 1, "CatBreed", "Manage_cat1");
		add_submenu_page("CatBreed", "Questions", "Questions", 1, "CatBreed", "Manage_instruction");
		add_submenu_page( 'CatBreed', 'Add Question', 'Add Question', '1',  'AddQuestion', 'addquestion' );
		add_submenu_page("CatBreed", "Cats", "Cats", 1, "Cats", "Manage_catslide2");
		add_submenu_page( 'CatBreed', 'Add Cat', 'Add Cat', '1',  'AddCat', 'catstep1' );
	}
	add_action('admin_menu', 'managegcat_admin_instruction');
}
if(isset($_REQUEST['cb']) && ($_REQUEST['cb']=='questionedit') )
{
	function Manage_catslide2() {
		include('editquestion.php');
	}
	function managegcat_admin_actions2() {
		add_menu_page("CatBreed", "Cat Breed", 1, "CatBreed", "Manage_cat1");
		add_submenu_page("CatBreed", "Questions", "Questions", 1, "CatBreed", "Manage_catslide2");
		add_submenu_page( 'CatBreed', 'Add Question', 'Add Question', '1',  'AddQuestion', 'addquestion' );
		add_submenu_page("CatBreed", "Cats", "Cats", 1, "Cats", "Manage_catslide2");
		add_submenu_page( 'CatBreed', 'Add Cat', 'Add Cat', '1',  'AddCat', 'catstep1' );
	}
	add_action('admin_menu', 'managegcat_admin_actions2');
}
if(isset($_REQUEST['cb']) && ($_REQUEST['cb']=='answers') )
{
	function Manage_answers() {
		include('listanswers.php');
	}
	function managegcat_admin_answers() {
		add_menu_page("CatBreed", "Cat Breed", 1, "CatBreed", "Manage_cat1");
		add_submenu_page("CatBreed", "Questions", "Questions", 1, "CatBreed", "Manage_answers");
		add_submenu_page( 'CatBreed', 'Add Question', 'Add Question', '1',  'AddQuestion', 'addquestion' );
		add_submenu_page("CatBreed", "Cats", "Cats", 1, "Cats", "Manage_catslide2");
		add_submenu_page( 'CatBreed', 'Add Cat', 'Add Cat', '1',  'AddCat', 'catstep1' );
	}
	add_action('admin_menu', 'managegcat_admin_answers');
}
if(isset($_REQUEST['cb']) && ($_REQUEST['cb']=='addanswer') )
{
	function Manage_addanswer() {
		include('addanswer.php');
	}
	function managegcat_admin_addanswer() {
		add_menu_page("CatBreed", "Cat Breed", 1, "CatBreed", "Manage_cat1");
		add_submenu_page("CatBreed", "Questions", "Questions", 1, "CatBreed", "Manage_addanswer");
		add_submenu_page( 'CatBreed', 'Add Question', 'Add Question', '1',  'AddQuestion', 'addquestion' );
		add_submenu_page("CatBreed", "Cats", "Cats", 1, "Cats", "Manage_catslide2");
		add_submenu_page( 'CatBreed', 'Add Cat', 'Add Cat', '1',  'AddCat', 'catstep1' );
	}
	add_action('admin_menu', 'managegcat_admin_addanswer');
}
if(isset($_REQUEST['cb']) && ($_REQUEST['cb']=='answeredit') )
{
	function Manage_answeredit() {
		include('answeredit.php');
	}
	function managegcat_admin_answeredit() {
		add_menu_page("CatBreed", "Cat Breed", 1, "CatBreed", "Manage_cat1");
		add_submenu_page("CatBreed", "Questions", "Questions", 1, "CatBreed", "Manage_answeredit");
		add_submenu_page( 'CatBreed', 'Add Question', 'Add Question', '1',  'AddQuestion', 'addquestion' );
		add_submenu_page("CatBreed", "Cats", "Cats", 1, "Cats", "Manage_catslide2");
		add_submenu_page( 'CatBreed', 'Add Cat', 'Add Cat', '1',  'AddCat', 'catstep1' );
	}
	add_action('admin_menu', 'managegcat_admin_answeredit');
}
if(isset($_REQUEST['cb']) && ($_REQUEST['cb']=='deleteanswer') )
{
	function Manage_deleteanswer() {
		include('deleteanswer.php');
	}
	function managegcat_admin_deleteanswer() {
		add_menu_page("CatBreed", "Cat Breed", 1, "CatBreed", "Manage_cat1");
		add_submenu_page("CatBreed", "Questions", "Questions", 1, "CatBreed", "Manage_deleteanswer");
		add_submenu_page( 'CatBreed', 'Add Question', 'Add Question', '1',  'AddQuestion', 'addquestion' );
		add_submenu_page("CatBreed", "Cats", "Cats", 1, "Cats", "Manage_catslide2");
		add_submenu_page( 'CatBreed', 'Add Cat', 'Add Cat', '1',  'AddCat', 'catstep1' );
	}
	add_action('admin_menu', 'managegcat_admin_deleteanswer');
}
if(isset($_REQUEST['cb']) && ($_REQUEST['cb']=='questiondelete') )
{
	function Manage_questiondelete() {
		include('questiondelete.php');
	}
	function managegcat_admin_questiondelete() {
		add_menu_page("CatBreed", "Cat Breed", 1, "CatBreed", "Manage_cat1");
		add_submenu_page("CatBreed", "Questions", "Questions", 1, "CatBreed", "Manage_questiondelete");
		add_submenu_page( 'CatBreed', 'Add Question', 'Add Question', '1',  'AddQuestion', 'addquestion' );
		add_submenu_page("CatBreed", "Cats", "Cats", 1, "Cats", "Manage_catslide2");
		add_submenu_page( 'CatBreed', 'Add Cat', 'Add Cat', '1',  'AddCat', 'catstep1' );
	}
	add_action('admin_menu', 'managegcat_admin_questiondelete');
}

if(isset($_REQUEST['cb']) && ($_REQUEST['cb']=='editcat') )
{
	function Manage_editcat() {
		include('editcat.php');
	}
	function managegcat_admin_editcat() {
		add_menu_page("CatBreed", "Cat Breed", 1, "CatBreed", "Manage_cat1");
		add_submenu_page("CatBreed", "Questions", "Questions", 1, "CatBreed", "Manage_cat1");
		add_submenu_page( 'CatBreed', 'Add Question', 'Add Question', '1',  'AddQuestion', 'addquestion' );
		add_submenu_page("CatBreed", "Cats", "Cats", 1, "Cats", "Manage_editcat");
		add_submenu_page( 'CatBreed', 'Add Cat', 'Add Cat', '1',  'AddCat', 'catstep1' );
	}
	add_action('admin_menu', 'managegcat_admin_editcat');
}

if(isset($_REQUEST['cb']) && ($_REQUEST['cb']=='catdelete') )
{
	function Manage_catdelete() {
		include('catdelete.php');
	}
	function managegcat_admin_catdelete() {
		add_menu_page("CatBreed", "Cat Breed", 1, "CatBreed", "Manage_cat1");
		add_submenu_page("CatBreed", "Questions", "Questions", 1, "CatBreed", "Manage_cat1");
		add_submenu_page( 'CatBreed', 'Add Question', 'Add Question', '1',  'AddQuestion', 'addquestion' );
		add_submenu_page("CatBreed", "Cats", "Cats", 1, "Cats", "Manage_catdelete");
		add_submenu_page( 'CatBreed', 'Add Cat', 'Add Cat', '1',  'AddCat', 'catstep1' );
	}
	add_action('admin_menu', 'managegcat_admin_catdelete');
}

if(isset($_REQUEST['cb']) && ($_REQUEST['cb']=='catattributes') )
{
	function Manage_catattributes() {
		include('catattributes.php');
	}
	function managegcat_admin_catattributes() {
		add_menu_page("CatBreed", "Cat Breed", 1, "CatBreed", "Manage_cat1");
		add_submenu_page("CatBreed", "Questions", "Questions", 1, "CatBreed", "Manage_cat1");
		add_submenu_page( 'CatBreed', 'Add Question', 'Add Question', '1',  'AddQuestion', 'addquestion' );
		add_submenu_page("CatBreed", "Cats", "Cats", 1, "Cats", "Manage_catattributes");
		add_submenu_page( 'CatBreed', 'Add Cat', 'Add Cat', '1',  'AddCat', 'catstep1' );
	}
	add_action('admin_menu', 'managegcat_admin_catattributes');
}

function managegcat_admin_actions() {
	
	add_menu_page("CatBreed", "Cat Breed", 1, "CatBreed", "Manage_cat1");
	add_submenu_page("CatBreed", "Questions", "Questions", 1, "CatBreed", "Manage_cat1");
	add_submenu_page( 'CatBreed', 'Add Question', 'Add Question', '1',  'AddQuestion', 'addquestion' );
	add_submenu_page("CatBreed", "Cats", "Cats", 1, "Cats", "Manage_catslide1");
	add_submenu_page( 'CatBreed', 'Add Cat', 'Add Cat', '1',  'AddCat', 'catstep1' );
}

if(!isset($_REQUEST['cb']) )
{
	add_action('admin_menu', 'managegcat_admin_actions');
}
function addquestion()
{
	include('addquestion.php');
}
function catstep1()
{
	include('addcat.php');
}

function CatDetail($id='', $cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if($id!='')
	{
		$cond.=" and id='$id'";
	}
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."cats where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}
function catattributes($id='', $catid='', $qid='', $ansid='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if($id!='')
	{
		$cond.=" and id='$id'";
	}
	if($catid!='')
	{
		$cond.=" and catid='$catid'";
	}
	if($qid!='')
	{
		$cond.=" and qid='$qid'";
	}
	if($ansid!='')
	{
		$cond.=" and ansid='$ansid'";
	}
	$querystr = "SELECT * FROM ".$prefix."catattributs where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}
function QustionDetail($id='', $cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if($id!='')
	{
		$cond.=" and id='$id'";
	}
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."questions where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}
function QustionAnswers($id='', $qid='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if($id!='')
	{
		$cond.=" and id='$id'";
	}
	if($qid!='')
	{
		$cond.=" and qid='$qid'";
	}
	$querystr = "SELECT * FROM ".$prefix."qanswers where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}
function instruction($id='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if($id!='')
	{
		$cond.=" and id='$id'";
	}
	$querystr = "SELECT * FROM ".$prefix."breedinstruction where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}
function catfeedsform($attr) 
{
	$questions=QustionDetail();
	$data='';
	$errors=array();
	if(count($questions)>0)
	{
		$showform=true;
		if(isset($_POST['submitcatbreed']))
		{
			$error=false;
			$cats=array();
			//echo'<pre>';
			$cnt=1;
			$instruction=instruction();
			foreach($questions as $question)
			{
				
				if(isset($_POST['option_'.$question->id]) && trim($_POST['option_'.$question->id])!='')
				{
					if($_POST['option_'.$question->id]!='donotcare')
					{
						$ansid=$_POST['option_'.$question->id];
						$ansvalues=QustionAnswers($ansid);
						
						$qid=$question->id;
						$attrs=catattributes('', '', $qid, $ansid);
						
						if(count($attrs)>0)
						{
							foreach($attrs as $attr)
							{
								$cats[$attr->catid]+=$ansvalues[0]->answervalue;
								
								if(isset($_POST['important_'.$question->id]) && trim($_POST['important_'.$question->id])!='')
								{
									$cats[$attr->catid]+=$ansvalues[0]->answervalue;
								}
							}
						}
					}
					else
					{
						if(isset($_POST['important_'.$question->id]) && trim($_POST['important_'.$question->id])!='')
						{
							$cnt++;
						}
					}
				}
				else
				{
					$error=true;
				}
				
			}
			
			if($error==false)
			{
				
				arsort($cats);
				$data.='<p style="font-weight: bold;">Our suggestions:</p>';
				if($cnt==1)
				{
					$totalresult=1;
					foreach ($cats as $key => $val) 
					{
						$percent=round($val/count($questions)*100,2);
						if($percent>100)
						{
							$percent=100;
						}
						$cats=CatDetail($key);
						$url=str_replace('http://','',$cats[0]->url);
						if($totalresult<=5)
						{
							$data.='<a href="http://'.$url.'" title="'.$cats[0]->name.'" target="_blank">'.$cats[0]->name.'</a> ('.$percent.'%)<br />';
						}
						$totalresult++;
					}
					$data.=$instruction[0]->instruction;
					$data.='<a href="'.get_permalink($post->ID).'" title="Go Back">Go Back</a>';
				}
				else
				{
					$data.=$instruction[0]->instruction;
					$data.=$instruction[0]->errorinstruction;
				}
				$showform=false;
			}
			else
			{
				array_push($errors,'Please answer to all the questions! Thank You!');
			}
			//echo'</pre>';
		}
		if(count($errors)>0){for($i=0;$i<count($errors);$i++){$data.='<p class="error">'.$errors[$i].'</p>';} }
		if($showform==true)
		{
			$data.='<form action="" method="post" name="catbreed" id="catbreed">';
			foreach($questions as $question)
			{
				$data.='<div class="qarea">';
					$data.='<p class="question">'.$question->question.'</p>';
					$answers=QustionAnswers('',$question->id);if(count($answers)>0){
						$data.='<div class="options">';
							$ansid='';
							if(isset($_POST['option_'.$question->id]))
							{
								$ansid=$_POST['option_'.$question->id];
							}
							foreach($answers as $answer){
								if($ansid==$answer->id)
								{
									$checked=' checked="checked"';
								}
								else
								{
									$checked='';
								}
								$data.='<div class="option">
										<input type="radio" name="option_'.$question->id.'"'.$checked.' value="'.$answer->id.'" id="option_'.$answer->id.'" />&nbsp;<label for="option_'.$answer->id.'">'.$answer->answer.'</label>
									</div>';
							}
						if($question->donotcare==1)
						{
							if($ansid=='donotcare')
							{
								$checked=' checked="checked"';
							}
							else
							{
								$checked='';
							}
							$data.='<div class="option">
										<input type="radio" name="option_'.$question->id.'"'.$checked.' value="donotcare" id="donot_'.$question->id.'" />&nbsp;<label for="donot_'.$question->id.'">I don\'t care</label>
									</div>';
						}
						if($question->important==1)
						{
							$checked2='';
							if(isset($_POST['important_'.$question->id]) && $_POST['important_'.$question->id]=='important')
							{
								$checked2=' checked="checked"';
							}
							$data.='<div class="option important">
										<input type="checkbox" name="important_'.$question->id.'"'.$checked2.' value="important" id="important_'.$question->id.'" />&nbsp;<label for="important_'.$question->id.'">important?</label>
									</div>';
						}
						$data.='</div>';
					}
				$data.='</div>';
			}
			$data.='<div class="submit-breed">
						<input type="submit" name="submitcatbreed" value="SUBMIT" />
					</div>
			</form>';
		}
	}
	return $data;
}
add_shortcode('catfeeds', 'catfeedsform');

function gcat_install() {
   global $wpdb;
   //global $product_db_version;


$sql = "CREATE TABLE `".$wpdb->prefix."questions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) DEFAULT NULL,
  `donotcare` char(1) DEFAULT 1,
  `important` char(1) DEFAULT 1,
  `importantvalue` int(5) DEFAULT 2,
  `active` char(1) DEFAULT 1,
  PRIMARY KEY (`id`)
)";
$sql2 = "CREATE TABLE `".$wpdb->prefix."qanswers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `qid` int(5) DEFAULT 0,
  `answer` varchar(255) DEFAULT NULL,
  `answervalue` int(2) DEFAULT 1,
  `orderby` int(10) DEFAULT 0,
  `active` char(1) DEFAULT 1,
  PRIMARY KEY (`id`)
)";

$sql3 = "CREATE TABLE `".$wpdb->prefix."cats` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `active` char(1) DEFAULT 1,
  PRIMARY KEY (`id`)
)";
$sql4 = "CREATE TABLE `".$wpdb->prefix."catattributs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `catid` int(5) DEFAULT 0,
  `qid` int(5) DEFAULT 0,
  `ansid` int(5) DEFAULT 0,
  PRIMARY KEY (`id`)
)";
$sql5 = "CREATE TABLE `".$wpdb->prefix."breedinstruction` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `instruction` longtext DEFAULT NULL,
  `errorinstruction` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
)";


   require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
   dbDelta($sql);
   dbDelta($sql2);
   dbDelta($sql3);
   dbDelta($sql4);
   dbDelta($sql5);
}
register_activation_hook(__FILE__,'gcat_install');
?>