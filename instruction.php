<?php 
	global $wpdb,$signature;
	$prefix=$wpdb->base_prefix;
	
	$catid='';
	if(isset($_REQUEST['catid']) && trim($_REQUEST['catid'])!='')
	{
		$catid=trim($_REQUEST['catid']);
	}
	$error=array();
	$instructions = instruction();
	$instruction=$instructions[0]->instruction;
	$errorinstruction=$instructions[0]->errorinstruction;
	if(isset($_POST['registration']))
	{
		$instruction=$_POST['instruction'];
		if(trim($instruction)=='')
		{
			array_push($error,'Please enter instruction');
		}
		$errorinstruction=$_POST['errorinstruction'];
		if(trim($errorinstruction)=='')
		{
			array_push($error,'Please enter error instruction');
		}
		if(count($error)<=0)
		{ 
			$result=$wpdb->query( "DELETE FROM `".$prefix."breedinstruction`" );
			$sql="INSERT INTO `".$prefix."breedinstruction` (`instruction`, `errorinstruction`) VALUES ('$instruction', '$errorinstruction')";
			$result = $wpdb->query( $sql );
			if($result==1)
			{
				$url=get_option('home').'/wp-admin/admin.php?page=CatBreed&cb=instruction&update=succ';
				echo"<script>window.location='".$url."'</script>";
			}
			
		}
	}
?>

<?php    echo "<h2>" . __( 'Manage Messages', 'webserve_trdom' ) . "</h2>"; ?>

<div class="clr"></div>
<?php if(isset($_REQUEST['del'])){if($_REQUEST['del']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Deleted successfully.' ); ?></strong></p></div>
<?php }} ?>
<?php if(isset($_REQUEST['add'])){if($_REQUEST['add']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Added successfully.' ); ?></strong></p></div>
<?php }} ?>
<?php if(isset($_REQUEST['update'])){if($_REQUEST['update']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Update successfully.' ); ?></strong></p></div>
<?php }} ?>
<div class="clr"></div>
<style type="text/css">
.error
{
	color:#CC0000;
}
.donotshowerror label.error
{
	display: none !important;
}
label.error
{
	margin-left:10px;
}
input.error, select.error,textarea.error, checkbox.error
{
	color:#000000;
	border:1px solid #CC0000 !important;
}
input[type='checkbox'].error
{
	border: solid #CC0000;
	outline:1px solid #CC0000 !important;
}
.personal_info{float:left; width:160px;}
.e-mail{ clear:both;}
.adress{ width:100px; float:left; text-align:left; font-size:13px; color:#454546;}
.field{ float:left; width:700px; height:400px;}
.profile .green-submit-btn input[type="submit"], .profile .green-submit-btn input[type="button"]{ width:152px; border:1px solid #b4babb; height: 45px; line-height:45px; text-align:center; color:#000; font-size:17px; font-weight:bold; border-radius:5px; display:block; font-family:Arial, Helvetica, sans-serif; cursor:pointer; }
.profile .green-submit-btn input[type="button"]{ margin-left:20px;}
.field .wp-core-ui input, .field .wp-core-ui select{ width:auto; height:auto;}
input, select, textarea{float:left;}
.clr{clear:both; margin-top:10px;}.mr5{margin-right:5px;}
.fl{float:left;}.removeday, .addday{float:left; color:#FF0000; font-size:18px; text-decoration:none; margin-left:10px;}.addday{color:#0000FF;}
.tt{float:left; width:70px;}
.sparator{width:600px; margin:5px 0px; height:1px; border-bottom:1px solid #000000;} 
.ml10{margin-left:10px;}
.field input[type="radio"]{width:20px; height:20px;}
</style>
	<div class="profile donotshowerror">
    	<?php if(count($error)>0){ ?>
		<div class="tabletitle"><span class="error">Error</span></div>
		<table width="700" class="from_main" border="0" cellpadding="0" cellspacing="0">
		  <?php 
		   
			for($i=0;$i<count($error);$i++){
				?>
			  <tr>
				<td align="left" valign="top" class="name"><span class="error"><?php echo $error[$i]; ?></span></td>
			</tr>
	<?php	} ?>
		</table>
		<div class="clr mt20"></div>
	 <?php } ?>
        <div class="right donotshowerror">
        	<form action="" method="post" name="register_spcialist" id="register_spcialist" enctype="multipart/form-data">
            <input type="hidden" name="catid" value="<?php _e($catid); ?>" />
                <div class="e-mail">
                    <div class="adress">Instruction:  </div>
                    <div class="field"><?php the_editor($instruction, 'instruction');?></div>
                </div>
                <div class="clr" style="margin-top:10px;"></div>
                <div class="e-mail">
                    <div class="adress" style="margin-top:70px;">Error instruction: </div>
                    <div class="field" style="margin-top:70px;"><?php the_editor($errorinstruction, 'errorinstruction');?></div>
                </div>
                <div class="clr"></div>
                
                <div class="e-mail">
                    <div class="adress">&nbsp;&nbsp;</div>
                    <div class="field" style="margin-top:70px;">
                        <div class="green-submit-btn">
                        	<input type="submit" name="registration" value="SUBMIT" class="registration_btn"/> <input onclick="return backtolist()" type="button" name="back" value="Back" title="Back" />
                       
                         </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
<div class="clr"></div>

<script type="text/javascript">
function backtolist()
{
	window.location='<?php echo get_option('home').'/wp-admin/admin.php?page=CatBreed'; ?>';
}
</script>

