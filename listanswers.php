<?php 
	global $wpdb,$signature;
	$prefix=$wpdb->base_prefix;
	$blog_id = $wpdb->blogid;
	
	$totalrec=20;
	$qid='';
	if(isset($_REQUEST['id']) && trim($_REQUEST['id'])!='')
	{
		$qid=trim($_REQUEST['id']);
	}
	
	$trips = QustionAnswers('', $qid);
	$question=QustionDetail($qid);
?>
<style type="text/css">
table td,table th{padding:5px;}
.pagination{ float:left; line-height:30px; font-size:14px; font-weight:bold;}
.pagination span{background:#f6f6f6; color:#000; padding:0px 10px; text-decoration:underline;}
.pagination a{background:#FFFFFF color:#0000FF; padding:0px 10px; text-decoration:none;}
.pagination a:hover{text-decoration:underline;}
ul.config{	padding:10px;	margin:0px;}
ul.config li{	display:inline;	float:left;	padding:0px 10px;}
ul.config li a{	text-decoration:none;	color:#000066;}
ul.config li a:hover, ul.config li a.active{	text-decoration:underline;	color:#990000;}
.clr{clear:both;}
.fl{float:left;}
.fr{float:right;}
</style>
<?php $url=get_option('home').'/wp-admin/admin.php?page=CatBreed'; ?>
<div class="wrap">
<?php    echo "<h2>" . __( 'Manage Answers', 'webserve_trdom' ) . "</h2>"; ?>

<div class="clr"></div>
<?php if(isset($_REQUEST['del'])){if($_REQUEST['del']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Deleted successfully.' ); ?></strong></p></div>
<?php }} ?>
<?php if(isset($_REQUEST['add'])){if($_REQUEST['add']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Added successfully.' ); ?></strong></p></div>
<?php }} ?>
<?php if(isset($_REQUEST['update'])){if($_REQUEST['update']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Update successfully.' ); ?></strong></p></div>
<?php }} ?>
<div class="clr"></div>
<div class="fl"><strong>Question :</strong> <?php _e($question[0]->question); ?></div>
<div class="fr" style="margin-right:30px;"><a href="<?php _e($url); ?>&cb=addanswer&qid=<?php _e($qid); ?>">Add Answer</a></div>
<div style="clear:both;"></div>
<form name="conatct_form" method="post" onSubmit="return check_blank();" action="<?php echo $url; ?>">
<input type="hidden" name="usr" value="filter" />
<div style="clear:both; height:20px;"></div>
	<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #ccc;">
		<tr>
			<th valign="top" align="left" width="60">&nbsp;<?php _e("Sr. No." ); ?></th>
			<th valign="top" align="left" style="border-left:1px solid #ccc;"><?php _e("Answer" ); ?></th>
			<th valign="top" align="left" style="border-left:1px solid #ccc;"><?php _e("Actions" ); ?></th>
		</tr>
	<?php $cnt=$limitstart+1; foreach($trips as $trip){ ?>
	  <tr>
		<td valign="top" align="left" style="border-top:1px solid #ccc;">&nbsp;<?php _e($cnt); ?></td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e($trip->answer); ?></td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">
            <a href="<?php _e($url); ?>&cb=answeredit&id=<?php _e($trip->id); ?>&qid=<?php _e($qid); ?>">View and Edit</a>&nbsp;&nbsp;
			<a href="javascript:if(confirm('Please confirm that you would like to delete this answer?')) {window.location='<?php _e($url); ?>&cb=deleteanswer&id=<?php _e($trip->id); ?>&qid=<?php _e($qid); ?>';}">Delete</a>
		</td>
	  </tr>
	  <?php $cnt++; } ?>
	</table>
</form>
</div>

